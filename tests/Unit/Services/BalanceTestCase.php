<?php

declare(strict_types = 1);

namespace Tests\Unit\Services;

use App\Utils\DatePeriod;
use Illuminate\Support\Collection;
use Illuminate\Support\Enumerable;
use Tests\TestCase;

abstract class BalanceTestCase extends TestCase
{
    protected function period(): \DatePeriod
    {
        return DatePeriod::create('2021-01-01', '2021-01-05');
    }

    protected function transactions(): Enumerable
    {
        return Collection::make([
            '2021-01-01' => 100,
            '2021-01-03' => -50,
            '2021-01-04' => 40,
        ]);
    }
}
