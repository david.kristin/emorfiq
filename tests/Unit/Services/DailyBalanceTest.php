<?php

declare(strict_types = 1);

namespace Tests\Unit\Services;

use App\Services\DailyBalance;

class DailyBalanceTest extends BalanceTestCase
{
    private DailyBalance $balance;

    protected function setUp(): void
    {
        parent::setUp();

        $this->balance = app(DailyBalance::class);
    }

    /** @test */
    public function it_returns_balances_by_days_with_zero_opening_balance(): void
    {
        $dailyBalance = $this->balance->calculate(0, $this->period(), $this->transactions());

        $this->assertEquals([
            '2021-01-01' => 100,
            '2021-01-02' => 100,
            '2021-01-03' => 50,
            '2021-01-04' => 90,
            '2021-01-05' => 90,
        ], $dailyBalance->toArray());
    }

    /** @test */
    public function it_returns_balances_by_days_with_a_non_zero_opening_balance(): void
    {
        $dailyBalance = $this->balance->calculate(100, $this->period(), $this->transactions());

        $this->assertEquals([
            '2021-01-01' => 200,
            '2021-01-02' => 200,
            '2021-01-03' => 150,
            '2021-01-04' => 190,
            '2021-01-05' => 190,
        ], $dailyBalance->toArray());
    }
}
