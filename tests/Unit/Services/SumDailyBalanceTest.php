<?php

declare(strict_types = 1);

namespace Tests\Unit\Services;

use App\Services\DailyBalance;
use App\Services\SumDailyBalance;

class SumDailyBalanceTest extends BalanceTestCase
{
    private SumDailyBalance $balance;

    protected function setUp(): void
    {
        parent::setUp();

        $this->balance = app(SumDailyBalance::class,  ['balance' => app(DailyBalance::class)]);
    }

    /** @test */
    public function it_returns_the_sum_of_daily_balances_with_zero_opening_balance(): void
    {
        $sumOfDailyBalances = $this->balance->calculate(0, $this->period(), $this->transactions());

        $this->assertEquals(430, $sumOfDailyBalances);
    }

    /** @test */
    public function it_returns_the_sum_of_daily_balances_with_a_non_zero_opening_balance(): void
    {
        $sumOfDailyBalances = $this->balance->calculate(100, $this->period(), $this->transactions());

        $this->assertEquals(930, $sumOfDailyBalances);
    }
}
