<?php

declare(strict_types = 1);

namespace Tests\Unit\Services;

use App\Services\AverageDailyBalance;
use App\Services\DailyBalance;
use App\Services\SumDailyBalance;

class AverageDailyBalanceTest extends BalanceTestCase
{
    private AverageDailyBalance $balance;

    protected function setUp(): void
    {
        parent::setUp();

        $this->balance = app(AverageDailyBalance::class, [
            'balance' => app(SumDailyBalance::class, [
                'balance' => app(DailyBalance::class),
            ]),
        ]);
    }

    /** @test */
    public function it_returns_an_average_daily_balance_with_zero_opening_balance(): void
    {
        $averageDailyBalance = $this->balance->calculate(0, $this->period(), $this->transactions());

        $this->assertEquals(86, $averageDailyBalance);
    }

    /** @test */
    public function it_returns_an_average_daily_balance_with_a_non_zero_opening_balance(): void
    {
        $averageDailyBalance = $this->balance->calculate(100, $this->period(), $this->transactions());

        $this->assertEquals(186, $averageDailyBalance);
    }
}
