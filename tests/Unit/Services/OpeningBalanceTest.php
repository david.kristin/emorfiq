<?php

declare(strict_types = 1);

namespace Tests\Unit\Services;

use App\Contracts\Repositories\TransactionRepository;
use App\Services\OpeningBalance;
use App\ValueObjects\AccountId;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Tests\TestCase;

class OpeningBalanceTest extends TestCase
{
    /** @test */
    public function it_sums_all_the_transactions(): void
    {
        $this->mock(TransactionRepository::class)
            ->shouldReceive('getBy')
            ->once()
            ->andReturn(
                Collection::make([
                    ['amount' => 30],
                    ['amount' => 80],
                    ['amount' => -20],
                ])
            );

        $openingBalance = (new OpeningBalance(app(TransactionRepository::class)))
            ->calculate(new AccountId(1), Carbon::now());

        $this->assertEquals(90, $openingBalance);
    }
}
