<?php

declare(strict_types = 1);

namespace Tests\Unit\Services;

use App\Contracts\BalanceFactory;
use App\Services\AverageDailyBalance;
use Tests\TestCase;

class BalanceFactoryTest extends TestCase
{
    /** @test */
    public function it_returns_a_decorated_object(): void
    {
        $factory = app(BalanceFactory::class);

        $this->assertInstanceOf(AverageDailyBalance::class, $factory->make());
    }
}
