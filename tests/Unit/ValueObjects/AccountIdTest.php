<?php

declare(strict_types = 1);

namespace Tests\Unit\ValueObjects;

use App\Exceptions\NegativeIdException;
use App\ValueObjects\AccountId;
use Tests\TestCase;

class AccountIdTest extends TestCase
{
    /** @test */
    public function an_id_equals_to_a_native_value(): void
    {
        $userId = new AccountId($id = 1);

        $this->assertEquals($id, $userId->toNative());
    }

    /** @test */
    public function an_id_must_be_a_positive_integer(): void
    {
        $this->expectException(NegativeIdException::class);

        new AccountId(-1);
    }
}
