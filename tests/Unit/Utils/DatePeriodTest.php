<?php

declare(strict_types = 1);

namespace Tests\Unit\Utils;

use App\Utils\DatePeriod;
use Tests\TestCase;

class DatePeriodTest extends TestCase
{
    private \DatePeriod $period;

    protected function setUp(): void
    {
        parent::setUp();

        $this->period = $this->period();
    }

    /** @test */
    public function it_sets_the_first_day_of_period(): void
    {
        $this->assertEquals('2021-01-01 00:00:00', $this->period->getStartDate());
    }

    /** @test */
    public function it_sets_the_last_day_of_period(): void
    {
        $this->assertEquals('2021-01-05 00:00:00', $this->period->getEndDate());
    }

    /** @test */
    public function it_includes_the_last_day_of_period(): void
    {
        $this->assertCount(4, $this->period->getIterator());
    }

    private function period(): \DatePeriod
    {
        return DatePeriod::create('2021-01-01', '2021-01-04');
    }
}
