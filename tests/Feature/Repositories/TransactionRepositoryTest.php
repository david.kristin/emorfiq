<?php

declare(strict_types = 1);

namespace Tests\Feature\Repositories;

use App\Models\Transaction;
use App\Repositories\Criteria\TransactionsBetweenDates;
use App\Repositories\Criteria\TransactionsByAccount;
use App\Repositories\Criteria\TransactionsGroupedByDate;
use App\Repositories\Criteria\TransactionsOlderThanSelectedDate;
use App\Repositories\TransactionRepository;
use App\ValueObjects\AccountId;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Illuminate\Support\Enumerable;
use Tests\FeatureTestCase;

class TransactionRepositoryTest extends FeatureTestCase
{
    use RefreshDatabase;

    private TransactionRepository $transactions;

    protected function setUp(): void
    {
        parent::setUp();

        $this->transactions = app(TransactionRepository::class);

        Carbon::setTestNow('2021-01-10 01:00:00');
    }

    /** @test */
    public function it_returns_transactions_between_dates(): void
    {
        $from = Carbon::now()->subWeek();
        $to = Carbon::now();

        $this->transactions(1, ['created_at' => Carbon::now()->subWeek()]);
        $this->transactions(1, ['created_at' => Carbon::now()->subDay()]);
        $this->transactions(1, ['created_at' => Carbon::now()]);
        $this->transactions(2, ['created_at' => Carbon::now()->subMonth()]);

        $transactions = $this->transactions->getBy(new TransactionsBetweenDates($from, $to));

        $this->assertDatabaseCount(Transaction::class, 5);
        $this->assertCount(3, $transactions);
        $this->assertEquals([1, 2, 3], $transactions->pluck('id')->toArray());
        $this->assertEquals(
            ['2021-01-03 01:00:00', '2021-01-09 01:00:00', '2021-01-10 01:00:00'],
            $transactions->pluck('created_at')->toArray()
        );
    }

    /** @test */
    public function it_returns_transactions_by_account(): void
    {
        $this->transactions(2, ['account_id' => 1]);
        $this->transactions(2, ['account_id' => 2]);

        $transactions = $this->transactions->getBy(new TransactionsByAccount(new AccountId(1)));

        $this->assertDatabaseCount(Transaction::class, 4);
        $this->assertCount(2, $transactions);
        $this->assertEquals([1, 2], $transactions->pluck('id')->toArray());
        $this->assertEquals([1, 1], $transactions->pluck('account_id')->toArray());
    }

    /** @test */
    public function it_returns_transactions_grouped_by_date(): void
    {
        $this->transactions(2, [
            'amount'     => 100,
            'created_at' => Carbon::now(),
        ]);
        $this->transactions(1, [
            'amount' => 50,
            'created_at' => Carbon::now()->addDays(10),
        ]);
        $this->transactions(1, [
            'amount' => 80,
            'created_at' => Carbon::now()->addDays(20),
        ]);

        $transactions = $this->transactions->getBy(new TransactionsGroupedByDate());

        $this->assertDatabaseCount(Transaction::class, 4);
        $this->assertCount(3, $transactions);
        $this->assertEquals([200, 50, 80], $transactions->pluck('amount')->toArray());
        $this->assertEquals(
            ['2021-01-10', '2021-01-20', '2021-01-30'],
            $transactions->pluck('date')->toArray()
        );
    }

    /** @test */
    public function it_returns_transactions_older_than_selected_date(): void
    {
        $this->transactions(2, ['created_at' => Carbon::now()->subDay()]);
        $this->transactions(1, ['created_at' => Carbon::now()]);
        $this->transactions(1, ['created_at' => Carbon::now()->addDay()]);

        $transactions = $this->transactions->getBy(new TransactionsOlderThanSelectedDate(Carbon::now()));

        $this->assertDatabaseCount(Transaction::class, 4);
        $this->assertCount(2, $transactions);
        $this->assertEquals([1, 2], $transactions->pluck('id')->toArray());
        $this->assertEquals(
            ['2021-01-09 01:00:00', '2021-01-09 01:00:00'],
            $transactions->pluck('created_at')->toArray()
        );
    }

    private function transactions(int $count, array $attributes = []): Enumerable
    {
        return Transaction::factory($count)->create($attributes);
    }
}
