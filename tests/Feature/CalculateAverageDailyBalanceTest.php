<?php

declare(strict_types = 1);

namespace Tests\Feature;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Symfony\Component\Console\Command\Command;
use Tests\FeatureTestCase;

class CalculateAverageDailyBalanceTest extends FeatureTestCase
{
    /** @test */
    public function it_requires_input_arguments(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Not enough arguments (missing: "accountId, from, to").');

        $this->artisan('calculate:balance');
    }

    /** @test */
    public function it_returns_an_average_daily_balance(): void
    {
        Transaction::factory(5)
            ->state(new Sequence(
                ['amount' => 100, 'created_at' => '2020-12-30'],
                ['amount' => -20, 'created_at' => '2021-01-01'],
                ['amount' => -30, 'created_at' => '2021-01-04'],
                ['amount' => 50, 'created_at' => '2021-01-07'],
                ['created_at' => '2021-02-08'],
            ))
            ->create(['account_id' => 1]);

        $this
            ->artisan('calculate:balance', [
                'accountId' => 1,
                'from'      => '2021-01-01',
                'to'        => '2021-01-07',
            ])
            ->expectsOutput('The average daily balance is: 70')
            ->assertExitCode(Command::SUCCESS);
    }
}
