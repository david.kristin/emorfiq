<?php

declare(strict_types = 1);

namespace App\Contracts;

use App\ValueObjects\AccountId;

interface Calculator
{
    public function calculate(AccountId $accountId, \DatePeriod $period): float;
}
