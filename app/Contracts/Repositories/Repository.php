<?php

declare(strict_types = 1);

namespace App\Contracts\Repositories;

use Illuminate\Support\Enumerable;

interface Repository
{
    public function getBy(Criterion $criteria): Enumerable;
}
