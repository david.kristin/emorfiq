<?php

declare(strict_types = 1);

namespace App\Contracts\Repositories;

use Illuminate\Database\Eloquent\Builder;

interface Criterion
{
    public function apply(Builder $builder): void;
}
