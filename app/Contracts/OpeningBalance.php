<?php

declare(strict_types = 1);

namespace App\Contracts;

use App\ValueObjects\AccountId;

interface OpeningBalance
{
    public function calculate(AccountId $accountId, \DateTimeInterface $date): float;
}
