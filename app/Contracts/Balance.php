<?php

declare(strict_types = 1);

namespace App\Contracts;

use Illuminate\Support\Enumerable;

interface Balance
{
    public function calculate(float $startBalance, \DatePeriod $period, Enumerable $transactions): Enumerable|float;
}
