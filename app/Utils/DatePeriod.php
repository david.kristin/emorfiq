<?php

declare(strict_types = 1);

namespace App\Utils;

use Carbon\CarbonImmutable;
use Carbon\CarbonPeriod;

final class DatePeriod
{
    public static function create(string $from, string $to): \DatePeriod
    {
        return (new CarbonPeriod)
            ->setDateClass(CarbonImmutable::class)
            ->setStartDate($from)
            ->setEndDate(CarbonImmutable::make($to)->addDay())
            ->toDatePeriod();
    }
}
