<?php

declare(strict_types = 1);

namespace App\Facades;

use App\Contracts\BalanceFactory;
use App\Contracts\Calculator;
use App\Contracts\Repositories\TransactionRepository;
use App\Models\Transaction;
use App\Repositories\Criteria\CriteriaCollection;
use App\Repositories\Criteria\TransactionsBetweenDates;
use App\Repositories\Criteria\TransactionsByAccount;
use App\Repositories\Criteria\TransactionsGroupedByDate;
use App\Services\OpeningBalance;
use App\ValueObjects\AccountId;

final class AverageDailyBalanceCalculator implements Calculator
{
    public function __construct(
        private TransactionRepository $transactions,
        private OpeningBalance        $openingBalance,
        private BalanceFactory        $balanceFactory,
    ) {}

    public function calculate(AccountId $accountId, \DatePeriod $period): float
    {
        $transactions = $this->transactions
            ->getBy(
                (new CriteriaCollection)
                    ->addCriterion(new TransactionsByAccount($accountId))
                    ->addCriterion(new TransactionsBetweenDates($period->getStartDate(), $period->getEndDate()))
                    ->addCriterion(new TransactionsGroupedByDate())
            )
            ->mapWithKeys(fn(Transaction $transaction) => [$transaction->date => $transaction->amount]);

        return $this->balanceFactory
            ->make()
            ->calculate(
                $this->openingBalance->calculate($accountId, $period->getStartDate()),
                $period,
                $transactions
            );
    }
}
