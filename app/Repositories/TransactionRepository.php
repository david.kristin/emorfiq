<?php

declare(strict_types = 1);

namespace App\Repositories;

use App\Contracts\Repositories\TransactionRepository as TransactionRepositoryContract;
use App\Models\Transaction;

final class TransactionRepository extends BaseRepository implements TransactionRepositoryContract
{
    public function model(): string
    {
        return Transaction::class;
    }
}
