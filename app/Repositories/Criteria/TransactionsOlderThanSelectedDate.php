<?php

declare(strict_types = 1);

namespace App\Repositories\Criteria;

use App\Contracts\Repositories\Criterion;
use Illuminate\Database\Eloquent\Builder;

class TransactionsOlderThanSelectedDate implements Criterion
{
    public function __construct(private \DateTimeInterface $date) {}

    public function apply(Builder $builder): void
    {
        $builder->whereDate('created_at', '<', $this->date);
    }
}
