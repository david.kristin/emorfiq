<?php

declare(strict_types = 1);

namespace App\Repositories\Criteria;

use App\Contracts\Repositories\Criterion;
use App\ValueObjects\AccountId;
use Illuminate\Database\Eloquent\Builder;

class TransactionsByAccount implements Criterion
{
    public function __construct(private AccountId $id) {}

    public function apply(Builder $builder): void
    {
        $builder->where('account_id', $this->id->toNative());
    }
}
