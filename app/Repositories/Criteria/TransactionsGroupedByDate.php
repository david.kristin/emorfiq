<?php

declare(strict_types = 1);

namespace App\Repositories\Criteria;

use App\Contracts\Repositories\Criterion;
use Illuminate\Database\Eloquent\Builder;

class TransactionsGroupedByDate implements Criterion
{
    public function apply(Builder $builder): void
    {
        $builder
            ->selectRaw('DATE(`created_at`) AS date, SUM(`amount`) AS amount')
            ->groupByRaw('DATE(`created_at`)');
    }
}
