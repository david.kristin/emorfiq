<?php

declare(strict_types = 1);

namespace App\Repositories\Criteria;

use App\Contracts\Repositories\Criterion;
use Illuminate\Database\Eloquent\Builder;

class CriteriaCollection implements Criterion
{
    /** @var array<int, Criterion> */
    private array $criteria;

    public function apply(Builder $builder): void
    {
        foreach ($this->criteria as $criterion) {
            $criterion->apply($builder);
        }
    }

    public function addCriterion(Criterion $criteria): self
    {
        $this->criteria[] = $criteria;

        return $this;
    }
}
