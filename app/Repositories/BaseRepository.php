<?php

declare(strict_types = 1);

namespace App\Repositories;

use App\Contracts\Repositories\Criterion;
use App\Contracts\Repositories\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Enumerable;

abstract class BaseRepository implements Repository
{
    private Model $model;

    public function __construct(private Application $app)
    {
        $this->model = $this->app->make($this->model());
    }

    public function getBy(Criterion $criteria): Enumerable
    {
        $criteria->apply($query = $this->model->newQuery());

        return $query->get();
    }

    abstract public function model(): string;
}
