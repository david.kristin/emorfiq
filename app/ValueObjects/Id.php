<?php

declare(strict_types = 1);

namespace App\ValueObjects;

use App\Exceptions\NegativeIdException;

abstract class Id
{
    use HasSingleValue;

    private int $value;

    public function __construct(int $value)
    {
        throw_if($value < 0, NegativeIdException::class);

        $this->value = $value;
    }

    public function toNative(): int
    {
        return $this->value;
    }
}
