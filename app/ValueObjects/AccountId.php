<?php

declare(strict_types = 1);

namespace App\ValueObjects;

use App\Contracts\ValueObject;

class AccountId extends Id implements ValueObject
{
    //
}
