<?php

declare(strict_types = 1);

namespace App\ValueObjects;

use App\Contracts\ValueObject;

trait HasSingleValue
{
    public function equals(ValueObject $object): bool
    {
        return $this->toNative() === $object->toNative();
    }
}
