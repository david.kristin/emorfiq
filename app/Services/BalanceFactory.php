<?php

declare(strict_types = 1);

namespace App\Services;

use App\Contracts\Balance;
use App\Contracts\BalanceFactory as BalanceFactoryContract;

final class BalanceFactory implements BalanceFactoryContract
{
    public function make(): Balance
    {
        return new AverageDailyBalance(
            new SumDailyBalance(
                new DailyBalance()
            )
        );
    }
}
