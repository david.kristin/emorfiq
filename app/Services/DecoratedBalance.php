<?php

declare(strict_types = 1);

namespace App\Services;

use App\Contracts\Balance;

abstract class DecoratedBalance
{
    public function __construct(protected Balance $balance) {}
}
