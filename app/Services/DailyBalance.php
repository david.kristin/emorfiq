<?php

declare(strict_types = 1);

namespace App\Services;

use App\Contracts\Balance;
use Illuminate\Support\Collection;
use Illuminate\Support\Enumerable;

final class DailyBalance implements Balance
{
    private float $currentBalance;

    public function calculate(float $startBalance, \DatePeriod $period, Enumerable $transactions): Enumerable
    {
        $this->currentBalance = $startBalance;

        return Collection::make($period)
            ->mapWithKeys(function (\DateTimeImmutable $date) use ($transactions): array {
                $balance = $transactions->get($date->format('Y-m-d'));
                $dailyBalance = transform($balance, fn() => $this->currentBalance, $this->currentBalance += $balance);

                return [$date->format('Y-m-d') => $dailyBalance];
            });
    }
}
