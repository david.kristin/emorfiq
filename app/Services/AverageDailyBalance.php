<?php

declare(strict_types = 1);

namespace App\Services;

use App\Contracts\Balance;
use Illuminate\Support\Enumerable;

final class AverageDailyBalance extends DecoratedBalance implements Balance
{
    public function calculate(float $startBalance, \DatePeriod $period, Enumerable $transactions): float
    {
        return $this->balance->calculate($startBalance, $period, $transactions) / iterator_count($period);
    }
}
