<?php

declare(strict_types = 1);

namespace App\Services;

use App\Contracts\OpeningBalance as OpeningBalanceContract;
use App\Contracts\Repositories\TransactionRepository;
use App\Repositories\Criteria\CriteriaCollection;
use App\Repositories\Criteria\TransactionsByAccount;
use App\Repositories\Criteria\TransactionsOlderThanSelectedDate;
use App\ValueObjects\AccountId;

final class OpeningBalance implements OpeningBalanceContract
{
    public function __construct(private TransactionRepository $transactions) {}

    public function calculate(AccountId $accountId, \DateTimeInterface $date): float
    {
        return $this->transactions
            ->getBy(
                (new CriteriaCollection)
                    ->addCriterion(new TransactionsByAccount($accountId))
                    ->addCriterion(new TransactionsOlderThanSelectedDate($date))
            )
            ->sum('amount');
    }
}
