<?php

declare(strict_types = 1);

namespace App\Exceptions;

class NegativeIdException extends \Exception
{
    //
}
